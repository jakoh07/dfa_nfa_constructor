class NFA(object):
    def __init__(self, states=[], sigma=[], start=[], delta={}, acc_states=[]):
        self.states=states
        self.sigma=sigma
        self.start=start
        self.delta=delta
        self.acc_states=acc_states
    #### Function to interactively define a NFA ###
    def define(self):
        print("DISCLAIMER: this little script things you know what you're doing, if you get a key error you defined your DEA wrongly!")
        states_str = input("Enter states seperatet by ',' (no spaces): ")
        self.states=list(states_str.split(','))
        self.sigma=list(input("Enter all letters of your Alphabet (no seperation): "))
        self.start=list(input("Enter start states seperated by ',' (no spaces): ").split(','))
        self.acc_states=list(input("Enter accepted final states seperated by ',' (no spaces):").split(","))
        print("Enter the rusulting states of your delta functions seperated by ',' (no spaces)")
        for q in self.states:
            self.delta.update({q:{}})
            for s in self.sigma:
                fxs=input("d("+q+", "+s+")=")
                if fxs:
                    fxs=list(fxs.split(','))
                else:
                    fxs=[]
                self.delta[q].update({s:fxs})
    def run(self, word):
        states_list=[]
        states_list.append(self.start)
        for s in word:
            states_list.append([])
            for q in states_list[-2]:
                if s in self.delta[q]:
                    for next_state in self.delta[q][s]:
                        states_list[-1].append(next_state)
        is_word = bool(set(states_list[-1]).intersection(self.acc_states))
        return (states_list, is_word)


class DFA(object):
    def __init__(self, states=[], sigma=[], start=None, delta={}, acc_states=[]):
        self.states=states
        self.sigma=sigma
        self.start=start
        self.delta=delta
        self.acc_states=acc_states
    #### Function to interactively define a DFA ###
    def define(self):
        print("DISCLAIMER: this little script things you know what you're doing, if you get a key error you defined your DEA wrongly!")
        states_str = input("Enter states seperatet by ',' (no spaces): ")
        self.states=list(states_str.split(','))
        self.sigma=list(input("Enter all letters of your Alphabet (no seperation): "))
        self.start=input("Enter start state: ")
        self.acc_states=list(input("Enter accepted final states seperated by ',' (no spaces):").split(","))
        print("Enter the rusulting state of your delta functions")
        for q in self.states:
            self.delta.update({q:{}})
            for s in self.sigma:
                self.delta[q].update({s:input("d("+q+", "+s+")=")})
    ### Function thet lets your DFA work through a string ###
    ### returns weather the word is accepted and the list ###
    ### of states in which the DFA has been. The first    ###
    ### is always the starting state, the second belongs  ###
    ### to the first character of the word, the thrird to ###
    ### the second and so on.                             ###
    def run(self, word):
        state=self.start
        states_list=[]
        states_list.append(state)
        for s in word:
            state=self.delta[state][s]
            states_list.append(state)
        return (states_list, state in self.acc_states)
    ### The function tests weather a word is accepted by your DFA ###
    def test(self, word):
        (l, r) = self.run(word)
        return r
